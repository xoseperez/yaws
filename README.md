# YAWS (Yet Another Weather Station)

Arduino FIO based weather station with:

* DHT22 temperature & humidity sensor 
* BMP085 barometric preassure sensor
* Anemometer
* Rain Gauge

The whole sensor is to be powered by a solar panel (doubling as irradiance sensor) and backed by a LiPo cell.
The data is transmitted to a gateway using a Ciseco XRF radio and LLAP protocol

## History

This is the second weather sensor I set up and as such is an evolution of the previous one. 
It adds wind and rain sensors using a couple of PCF8583 as counters and uses LLAP protocol to send messages
to a gateway unit using XRF radios.

## Dependencies

* [Rocket Scream Lightweight Low Power Arduino Library][1]
* [Ben Adams' Arduino library for the DHT22 humidity and temperature sensor][2]
* [Adafruit BMP085 Library][3]
* Tinkerkit secret [voltmeter][4] & [thermometer][5] for the Arduino 328
* [PCF8583 RTC and Event Counter Library for Arduino][6]
* [LLAPSerial Protocol Library for Arduino][7]

[1]: https://github.com/rocketscream/Low-Power
[2]: https://github.com/ringerc/Arduino-DHT22
[3]: https://github.com/adafruit/Adafruit-BMP085-Library
[4]: http://code.google.com/p/tinkerit/wiki/SecretVoltmeter
[5]: http://code.google.com/p/tinkerit/wiki/SecretThermometer
[6]: https://bitbucket.org/xoseperez/pcf8583
[7]: https://bitbucket.org/xoseperez/llapserial

