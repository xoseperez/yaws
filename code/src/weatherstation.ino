/*

  Yet Another Weather Station
  Copyright (C) 2013 by Xose Pérez <xose dot perez at gmail dot com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <LowPower.h>
#include <DHT22.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <LLAPSerial.h>
#include <PCF8583.h>
#include <Magnitude.h>
#include <MovingData.h>
#include <MemoryFree.h>

// DHT22 connections:
// Connect pin 1 (on the left) of the sensor to 3.3V
// Connect pin 2 of the sensor to whatever your DHT_PIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// BMP085 and PCF8586 connections:
// Connect VCC of the sensor to 3.3V (NOT 5.0V!)
// Connect GND to Ground
// Connect SCL to i2c clock - on '168/'328 Arduino Uno/Duemilanove/etc thats Analog 5
// Connect SDA to i2c data - on '168/'328 Arduino Uno/Duemilanove/etc thats Analog 4
// EOC is not used, it signifies an end of conversion
// XCLR is a reset pin, also not used here

// ===========================================
// Configuration
// ===========================================

#define BAUD_RATE 9600

#define BATT_PIN 0
#define PANEL_PIN 1
#define RADIO_SLEEP_PIN 4
#define DHT_PIN 5
#define ANEMOMETER_ADDRESS 0xA0
#define RAIN_GAUGE_ADDRESS 0xA2
#define NOTIFICATION_PIN 13

#define DHT_TYPE DHT22
#define VOLTAGE_REFERENCE_VALUE 1100
#define VOLTAGE_REFERENCE_CODE INTERNAL
#define BATT_VOLTAGE_FACTOR 4.28 // 99.8KOhm + 327.4KOhm
#define PANEL_VOLTAGE_FACTOR 9.09 // 100.2kOhm + 811kOhm

#define SLEEP_INTERVAL SLEEP_4S
#define SLEEP_INTERVAL_SECONDS 4
#define MEASURE_EVERY 13
#define SEND_EVERY 65 // that's 13*5+1
#define WARMUP_DELAY 2000
#define RADIO_DELAY 100
#define PCF8583_MAX_COUNTS_PER_SECOND 50 // 50Hz ~ 125km/h

//#define DEBUG
#include "debug.h"

// ===========================================
// Globals
// ===========================================

DHT22 dht(DHT_PIN);
Adafruit_BMP085 bmp;
LLAPSerial LLAP(Serial);
PCF8583 anemometer(ANEMOMETER_ADDRESS);
PCF8583 rain_gauge(RAIN_GAUGE_ADDRESS);

boolean bmp_ready = false;
boolean dht_ready = true;
int interval = 0;

Magnitude dht22_temperature;
Magnitude dht22_humidity;
Magnitude bmp085_pressure;
Magnitude bmp085_temperature;
Magnitude battery_voltage;
Magnitude panel_voltage;
Magnitude anemometer_count;
MovingData rain_hourly_count(12);

// ===========================================
// Methods
// ===========================================

/*
 * radioSleep
 * Sets the radio to sleep
 *
 * @return void
 */
void radioSleep() {
    DEBUG_PRINT(F("radioSleep"));
    delay(RADIO_DELAY);
    digitalWrite(RADIO_SLEEP_PIN, HIGH);
    digitalWrite(NOTIFICATION_PIN, LOW);
}

/*
 * radioWake
 * Wakes up the radio to sleep
 *
 * @return void
 */
void radioWake() {
    DEBUG_PRINT(F("radioWake"));
    digitalWrite(NOTIFICATION_PIN, HIGH);
    digitalWrite(RADIO_SLEEP_PIN, LOW);
    delay(RADIO_DELAY);
}

/*
 * readVoltage
 * Reads the analog PIN and performs the conversion to get mV
 *
 * @param byte pin PIN number
 * @param float factor conversion factor based on voltage divider
 * @return long
 */
long readVoltage(byte pin, float factor) {
    DEBUG_PRINT(F("readVoltage"));
    int reading = analogRead(pin);
    DEBUG_PRINT(reading)
    return (long) map(reading, 0, 1023, 0, VOLTAGE_REFERENCE_VALUE) * factor;
}

/*
 * readAnemometer
 *
 * Reads anemometer counter
 */
void readAnemometer() {

    DEBUG_PRINT(F("readAnemometer"));

    static unsigned long previous = 0;
    unsigned long current = anemometer.getCount();
    DEBUG_PRINT(current);

    unsigned long delta;
    if (current >= previous) {
        delta = current - previous;
    } else {
        delta = 1000000L - previous + current;
    }
    DEBUG_PRINT(delta);

    // If the count exceeds 50 events per second I don't trust the result
    // (mind the counter counts double)
    if (delta < PCF8583_MAX_COUNTS_PER_SECOND * SLEEP_INTERVAL_SECONDS * 2) {
        anemometer_count.store((float) delta);
    } else {
        DEBUG_PRINT(F("Count out of range"));
    }

    previous = current;

}

/*
 * readRainGauge
 *
 * Reads rain count gauge counter
 */
unsigned long readRainGauge() {

    DEBUG_PRINT(F("readRainGauge"));

    static unsigned long previous = 0;
    unsigned long current = rain_gauge.getCount();
    DEBUG_PRINT(current);

    unsigned long delta;
    if (current >= previous) {
        delta = current - previous;
    } else {
        delta = 1000000L - previous + current;
    }
    DEBUG_PRINT(delta);

    // If the count exceeds 50 events per second I don't trust the result
    // (mind the counter counts double)
    if (delta >= PCF8583_MAX_COUNTS_PER_SECOND * SLEEP_INTERVAL_SECONDS * 2) {
        delta = 9999;
        DEBUG_PRINT(F("Count out of range"));
    }

    previous = current;
    return delta;

}
/*
 * readDHT22
 * Reads humidity and temperature from DHT22
 *
 * @return void
 */
void readDHT22() {

    DEBUG_PRINT(F("readDHT22"));

    // Allowing the DHT22 to warm up
    delay(WARMUP_DELAY);

    DHT22_ERROR_t errorCode = dht.readData();
    DEBUG_PRINT(errorCode);
    if (errorCode == DHT_ERROR_NONE) {
        dht22_temperature.store(dht.getTemperatureC());
        DEBUG_PRINT(dht.getTemperatureC());
        dht22_humidity.store(dht.getHumidity());
        DEBUG_PRINT(dht.getHumidity());
    } else {
        DEBUG_PRINT(F("DHT22 out of reach"))
        dht_ready = false;
    }


}

/*
 * intiBMP085
 * Initilizes BMP085
 *
 * @return void
 */
void initBMP085() {
    bmp_ready = (bool) bmp.begin(BMP085_ULTRAHIGHRES);
    if (!bmp_ready) {
        DEBUG_PRINT(F("BMP085 out of reach"))
    }
}

/*
 * readBMP085
 * Reads pressure and temperature from BMP085
 *
 * @return void
 */
void readBMP085() {

    DEBUG_PRINT(F("readBMP085"));

    if (!bmp_ready) initBMP085();
    if (bmp_ready) {

        float temp = bmp.readTemperature();
        DEBUG_PRINT(temp);
        if (-10 < temp && temp < 50) {
            bmp085_temperature.store(temp);
        } else {
            // Out of bounds value for BMP temperature,
            // discard and reinitialize sensor
            DEBUG_PRINT(F("Temperature out of range"));
            bmp_ready = false;
        }

        float pres = bmp.readPressure();
        DEBUG_PRINT(pres);
        if (90000 < pres && pres < 1100000) {
            bmp085_pressure.store(pres);
        } else {
            // Out of bounds value for BMP presure,
            // discard and reinitialize sensor
            DEBUG_PRINT(F("Pressure out of range"));
            bmp_ready = false;
        }

    }
}

/*
 * readVoltages
 * Reads voltages for battery and solar panel
 *
 * @return void
 */
void readVoltages() {
    DEBUG_PRINT(F("readVoltages"));
    battery_voltage.store((float) readVoltage(BATT_PIN, BATT_VOLTAGE_FACTOR));
    panel_voltage.store((float) readVoltage(PANEL_PIN, PANEL_VOLTAGE_FACTOR));
}

/*
 * resetAll
 * Resets all averages
 *
 * @return void
 */
void resetAll() {
    DEBUG_PRINT(F("resetAll"));
    dht22_humidity.reset();
    dht22_temperature.reset();
    bmp085_temperature.reset();
    bmp085_pressure.reset();
    battery_voltage.reset();
    panel_voltage.reset();
    anemometer_count.reset();
}

/*
 * sendAll
 * Gets averages and sends all data through UART link
 *
 * @return void
 */
void sendAll() {

    DEBUG_PRINT(F("sendAll"));

    radioWake();

    if (dht22_temperature.count() > 3) {
        float tmp1 = dht22_temperature.average();
        DEBUG_PRINT(tmp1);
        LLAP.sendMessage("T1", tmp1, 2);
        delay(RADIO_DELAY);
    }

    if (dht22_humidity.count() > 3) {
        float humi = dht22_humidity.average();
        DEBUG_PRINT(humi);
        LLAP.sendMessage("RH", humi, 2);
        delay(RADIO_DELAY);
    }

    if (bmp085_temperature.count() > 3) {
        float tmp2 = bmp085_temperature.average();
        DEBUG_PRINT(tmp2);
        LLAP.sendMessage("T2", tmp2, 2);
        delay(RADIO_DELAY);
    }

    if (bmp085_pressure.count() > 3) {
        float pres = bmp085_pressure.average() / 100.0; // in hPa
        DEBUG_PRINT(pres);
        LLAP.sendMessage("PS", pres, 1);
        delay(RADIO_DELAY);
    }

    // 1c/s = 2.5km/h, the buckets are SLEEP_INTERVAL_SECONDS seconds wide, and it counts double!!
    if (anemometer_count.count() > 3) {
        float wind = anemometer_count.average() * 5 / 2 / SLEEP_INTERVAL_SECONDS;
        DEBUG_PRINT(wind);
        LLAP.sendMessage("WD", wind, 1);
        delay(RADIO_DELAY);
    }

    // 1c/s = 2.5km/h, the buckets are SLEEP_INTERVAL_SECONDS seconds wide, and it counts double!!
    if (anemometer_count.count() > 1) {
        float wndx = anemometer_count.maximum() * 5 / 2 / SLEEP_INTERVAL_SECONDS;
        DEBUG_PRINT(wndx);
        LLAP.sendMessage("WX", wndx, 1);
        delay(RADIO_DELAY);
    }

    // 0.3mm per count, the counter counts double!!
    unsigned long rain = readRainGauge();
    if (rain != 9999) {

        LLAP.sendMessage("R1", (float) rain * 3 / 20, 1);
        delay(RADIO_DELAY);

        rain_hourly_count.store(rain);
        DEBUG_PRINT(rain_hourly_count.sum());
        LLAP.sendMessage("R2", (float) rain_hourly_count.sum() * 3 / 20, 1);
        delay(RADIO_DELAY);
    }

    float bat1 = battery_voltage.average();
    DEBUG_PRINT(bat1);
    LLAP.sendMessage("B1", bat1, 0);
    delay(RADIO_DELAY);

    float bat2 = panel_voltage.average();
    DEBUG_PRINT(bat2);
    LLAP.sendMessage("B2", bat2, 0);
    delay(RADIO_DELAY);

    radioSleep();

}

/*
 * setup
 *
 * @return void
 */
void setup() {

    // Using the ADC against internal 1V1 reference for battery monitoring
    analogReference(VOLTAGE_REFERENCE_CODE);

    // Initialize UART, LLAP
    Serial.begin(BAUD_RATE);
    LLAP.setLocalID("AB");

    // Link radio
    pinMode(RADIO_SLEEP_PIN, OUTPUT);
    pinMode(NOTIFICATION_PIN, OUTPUT);
    radioWake();
    delay(1000);
    LLAP.sendMessage("ST", "1");
    delay(RADIO_DELAY);
    radioSleep();

    // Initialize PCF8583
    anemometer.setMode(MODE_EVENT_COUNTER);
    anemometer.setCount(0);
    rain_gauge.setMode(MODE_EVENT_COUNTER);
    rain_gauge.setCount(0);

}

/*
 * loop
 *
 * @return void
 */
void loop() {

    ++interval;
    DEBUG_PRINT(interval);

    // Always take a wind measure
    readAnemometer();

    // Now, every MEASURE_EVERY intervals (1 minute) take the rest of measures
    if (interval % MEASURE_EVERY == 0) {
        readDHT22();
        readBMP085();
        readVoltages();
    }

    // And every SEND_EVERY intervals (5 minutes) send messages
    if (interval % SEND_EVERY == 0) {
        sendAll();
        resetAll();
        interval = 0;
    }

    DEBUG_PRINT(freeMemory());

    DEBUG_PRINT(F("OFF"));
    LowPower.powerDown(SLEEP_INTERVAL, ADC_OFF, BOD_OFF);
    DEBUG_PRINT(F("ON"));

}
